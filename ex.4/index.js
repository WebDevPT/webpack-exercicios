// object definition example
const produto = {
    nome: 'Toshiba Laptop',
    price: 990,
    discount: 0.05,
};

// spread operator
function clone(object) {
    // returns a new object with spread operator
    return { ...object };
};

// create new object using clone function wich returns a new object
const novoProduto = clone(produto);
// change atribute for novoProduto object
novoProduto.nome = 'Toshiba Qosmio Laptop';
// print produto and novoProduto
console.log(produto, novoProduto);
// print produto atribute 'nome'
console.log(produto.nome);
// print novoProduto atribute 'nome'
console.log(novoProduto.nome);

// Array vs Object EXAMPLES

// object definition example
const novoObjeto = {
    atributo1: "atribute info",
    atribute2: 10,
    atribute3: true,
    atribute4: "Test 123",
};

// print object
console.log(novoObjeto);

// array definition example
const novoArray = ['atribute info', 10, true, 'Test 123'];
// print all array elements
console.log(`Array elements: ${novoArray}`);

// print array element position 2
// note that arrays start at position 0, so element 2 ('10') is at position 1 ([0,1,2,...])
console.log(`Array element [2]: ${novoArray[1]}`);
